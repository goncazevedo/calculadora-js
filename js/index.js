var tela = document.getElementById("display")
var nums = document.getElementsByClassName("num")
var ops = document.getElementsByClassName("op")

tela.innerText = ""

for (let i = 0; i < nums.length; i++) {
    nums[i].addEventListener("click", function(events){
        tela.innerText += nums[i].innerText
    })

    if (i<4) {
        ops[i].addEventListener("click", function(events){
            tela.innerText += ops[i].innerText
        })
    }
    
}


function calculate(tela) {
    var c = tela.innerText
    
    if (c.includes("+")){
        var ns = c.split("+")
        ns[0] = parseFloat(ns[0],10)
        ns[1] = parseFloat(ns[1],10)
        
        var op = ns[0] + ns[1];
        tela.innerText = op;
    }
    else if (c.includes("-")){
        var ns = c.split("-")
        ns[0] = parseFloat(ns[0],10)
        ns[1] = parseFloat(ns[1],10)
        
        var op = ns[0] - ns[1];
        tela.innerText = op;
    }
    else if (c.includes("*")){
        var ns = c.split("*")
        ns[0] = parseFloat(ns[0],10)
        ns[1] = parseFloat(ns[1],10)
        
        var op = ns[0] * ns[1];
        tela.innerText = op;
    }
    else if (c.includes("/")){
        var ns = c.split("/")
        ns[0] = parseFloat(ns[0],10)
        ns[1] = parseFloat(ns[1],10)
        if (ns[1] == 0){
            tela.innerText = "ERRO";
        }
        else{
            var op = ns[0] / ns[1];
            tela.innerText = op;
        }
    }
}

function limpar(tela){
    tela.innerText = "";
}